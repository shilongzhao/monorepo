

alpha version

TO run the app with Bazel
```aidl
bazel run //api:api_app
```

We can specify run time configuration file by adding `executable flags` or by appending `-conf /path/to/config.json` when running the jar file.

Build a fat jar and run it [1](https://stackoverflow.com/a/58053031/15360746)
```shell
 bazel build //gateway:api_app_deploy.jar
 java -jar bazel-bin/gateway/api_app_deploy.jar -conf /path/to/json
```

FAQs: 

1. IntelliJ does not recognize the external dependencies (no code autocomplete, etc) 
- solution: Add build target to the `targets` section in `.bazelproject` and run `Bazel -> Sync -> Sync Working Set` and `Sync Project with BUILD files` [3](https://ij.bazel.build/docs/project-views.html)

2. Error while extending `CoroutineVertile`

```
Cannot access 'kotlinx.coroutines.CoroutineScope' which is a supertype of 'gateway.src.gateway.main.kotlin.gateway.MainVerticle'. 
Check your module classpath for missing or conflicting dependencies
```

- solution: The `kotlin_coroutines` library seems not imported, add `kotlinx.coroutines.core` to WORKSPACE and BUILD files, see [2](https://github.com/bazelbuild/rules_jvm_external#resolving-user-specified-and-transitive-dependency-version-conflicts) for transitive dependencies


1. Use `io.vertx.core.Launcher` as MainClass and run the `gateway` application.

- Solution: this is achieved by overriding the `getMainVerticle` method in the `Launcher` 

1. JUnit test not working, test not found, etc 

- Solution: include the `        "org.junit.platform:junit-platform-console:jar:1.7.0",
` and add ` main_class = "org.junit.platform.console.ConsoleLauncher",
               args = [
                   "--select-package=gateway.test",
               ],` in `kt_jvm_test`


1. `Package directive doesn't match file location` 
- solution: added package information. other solutions: go to the IntelliJ IDEA module settings remove the root dir `.` from `sources root`

Run with external configuration 

```
java -jar bazel-bin/gateway/api_app_deploy.jar -conf gateway/resources/config.json
```

- gRPC support [rules_protobuf](https://github.com/grpc/grpc-java) and [google discuss](https://groups.google.com/g/bazel-discuss/c/75DSeUTYBxE/m/su67cm_7BQAJ)


2. logback configuration does not work for color [ref:logback](http://logback.qos.ch/manual/mdc.html)   
- solution: [IntelliJ Specific] wrong module structure, remove the root directories from sources root in module settings[see here](https://github.com/bazelbuild/intellij/issues/437)

TODO:
1. Docker?
2. Kubernetes?
3. OpenAPI Contract
4. JWT Token
5. Login/OAuth2

```
Error:(69, 12) Building api/api_app-class.jar () failed: (Exit 1): java failed: error executing command external/remotejdk11_macos/bin/java -XX:+UseParallelOldGC -XX:-CompactStrings '--patch-module=java.compiler=external/remote_java_tools_darwin/java_tools/java_compiler.jar' ... (remaining 15 argument(s) skipped)
```
#!/bin/zsh
openssl genrsa -out CAroot.key 2048
openssl req -new -key CAroot.key -out CAroot.csr # CN should be different from the certificates below
openssl req -x509 -days 1825 -key CAroot.key -in CAroot.csr -out CAroot.crt
cat CAroot.crt CAroot.key > CAroot.pem

openssl genrsa -out gateway.key 2048
openssl req -new -key gateway.key -out gateway.csr
openssl x509 -req -days 1825 -in gateway.csr -CA CAroot.pem -CAkey CAroot.key -CAcreateserial -out gateway.crt
cat gateway.crt gateway.key > gateway.pem

#openssl genrsa -out client.key 2048
#openssl req -new -key client.key -out client.csr
#openssl x509 -req -days 1825 -in client.csr -CA CAroot.pem -CAkey CAroot.key -CAcreateserial -out client.crt
#cat client.crt client.key > client.pem

# mongo --ssl --sslCAFile CAroot.pem --sslPEMKeyFile client.pem --authenticationDatabase production -u user -p password --host mongo.host --port 27018


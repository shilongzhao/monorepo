You can generate the CA certs and Server certs using the shell script here
then you need to set up your browsers to trust these self signed certs

Or you can user `sslforfree.com` to generate a cert for you (which requires that you own a domain, here I use www.szhao.org and points it to localhost in my /etc/hosts ) and then it will be possible to access `https://www.szhao.org:8080/` without any security warnings from the browser.
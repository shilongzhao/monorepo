package org.szhao

import io.grpc.Status
import io.grpc.StatusException
import io.grpc.stub.StreamObserver
import org.slf4j.LoggerFactory
import proto.Messages
import proto.ProductInfoGrpc
import java.util.*
import kotlin.collections.HashMap

class ProductService: ProductInfoGrpc.ProductInfoImplBase() {
    private val logger = LoggerFactory.getLogger(ProductService::class.java)

    private val productMap = HashMap<String, Messages.Product>()

    override fun addProduct(request: Messages.Product, response: StreamObserver<Messages.ProductID>) {
        logger.info("received add product request $request")
        val uuid = UUID.randomUUID()
        productMap[uuid.toString()] = request;
        val id = Messages.ProductID.newBuilder().setValue(uuid.toString()).build()
        response.onNext(id)
        response.onCompleted()
    }

    override fun getProduct(request: Messages.ProductID, response: StreamObserver<Messages.Product>) {
        logger.info("received get product request $request")
        val id = request.value
        val product = productMap[id]
        if (product != null) {
            response.onNext(product)
            response.onCompleted()
        } else {
            response.onError(StatusException(Status.NOT_FOUND))
        }
    }
}
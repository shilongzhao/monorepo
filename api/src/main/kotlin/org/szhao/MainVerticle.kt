@file:JvmName("MainVerticle")
package org.szhao
import io.vertx.kotlin.core.deploymentOptionsOf
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.await
import org.slf4j.LoggerFactory

class MainVerticle: CoroutineVerticle() {
    companion object {
        private val logger = LoggerFactory.getLogger(MainVerticle::class.java)
    }
    override suspend fun start() {
        val options = deploymentOptionsOf(config = config, instances = 2)
        vertx.deployVerticle(GRPCVerticle::class.java, options).await() // await deployment without blocking event loop
        vertx.deployVerticle(HttpServerVerticle::class.java, options).await()
        logger.info("main verticle started with config ${config.toString().substring(0, 15)}...")
    }

    override suspend fun stop() {
       logger.info("main verticle fading")
    }
}
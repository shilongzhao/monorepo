@file:JvmName("Main")

package org.szhao

import org.slf4j.LoggerFactory

fun main(args: Array<String>) {
    val logger = LoggerFactory.getLogger("org.szhao.Main")
    logger.info("args: ${args.joinToString(separator = ", ")}")
    AppLauncher().dispatch(args)
}
@file:JvmName("AppLauncher")

package org.szhao
import io.vertx.core.Launcher

class AppLauncher: Launcher() {
    /**
     * specifies the simple name of the main verticle of our app
     */
    override fun getMainVerticle(): String = MainVerticle::class.java.name
}


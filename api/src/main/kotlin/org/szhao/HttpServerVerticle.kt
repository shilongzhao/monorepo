package org.szhao

import io.vertx.core.http.HttpMethod
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.Router
import io.vertx.ext.web.RoutingContext
import io.vertx.ext.web.handler.BodyHandler
import io.vertx.ext.web.handler.CSRFHandler
import io.vertx.ext.web.handler.SessionHandler
import io.vertx.ext.web.sstore.LocalSessionStore
import io.vertx.kotlin.core.http.httpServerOptionsOf
import io.vertx.kotlin.core.json.get
import io.vertx.kotlin.core.net.pemKeyCertOptionsOf
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.await
import org.slf4j.LoggerFactory

class HttpServerVerticle: CoroutineVerticle() {
    companion object {
        private val logger = LoggerFactory.getLogger(HttpServerVerticle::class.java)
        private const val KB = 1024L
        private const val MB = 1024 * KB
    }

    private val secureHeaderHandler: (RoutingContext) -> Unit = { ctx ->
        ctx.response().apply {
            // do not allow proxies to cache the data
            putHeader("Cache-Control", "no-store, no-cache")
            // prevents Internet Explorer from MIME - sniffing a
            // response away from the declared content-type
            putHeader("X-Content-Type-Options", "nosniff")
            // Strict HTTPS (for about ~6Months)
            putHeader("Strict-Transport-Security", "max-age=" + 15768000)
            // IE8+ do not allow opening of attachments in the context of this resource
            putHeader("X-Download-Options", "noopen")
            // enable XSS for IE
            putHeader("X-XSS-Protection", "1; mode=block")
            // deny frames
            putHeader("X-FRAME-OPTIONS", "DENY")
        }
        ctx.next()
    }

    override suspend fun start() {
        val httpConf: JsonObject = config["http_server"]
        val keyPath: String = httpConf["ssl_key_file"]
        val certPath: String = httpConf["ssl_cert_file"]

        val port: Int = httpConf["port"]
        val host: String = httpConf["host"]
        val key = vertx.fileSystem().readFile(keyPath).await()
        val cert = vertx.fileSystem().readFile(certPath).await()

        val pemKeyCertOptions = pemKeyCertOptionsOf(keyValue = key, certValue = cert)
        val httpServerOptions = httpServerOptionsOf(ssl = true, pemKeyCertOptions = pemKeyCertOptions)
        val router = Router.router(vertx)

        val cookieHandler = SessionHandler.create(LocalSessionStore.create(vertx))
            .setCookieHttpOnlyFlag(true).setCookieSecureFlag(true)
        val csrfHandler = CSRFHandler.create(vertx, "{!!secret_csrf@replace!!}")
        val bodyHandler = BodyHandler.create().setBodyLimit(10 * MB)

        router.route()
            .handler(secureHeaderHandler)
            .handler(cookieHandler)
            .handler(bodyHandler)
            .handler(csrfHandler)

        router.route(HttpMethod.GET, "/").handler {
            it.response().end("welcome!")
        }

        vertx.createHttpServer(httpServerOptions).requestHandler(router).listen(port, host).await()
        logger.info("started https server")

    }
}
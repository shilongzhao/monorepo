package org.szhao

import io.vertx.grpc.VertxServer
import io.vertx.grpc.VertxServerBuilder
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.awaitResult
import org.slf4j.LoggerFactory

class GRPCVerticle: CoroutineVerticle() {
    companion object {
        private val logger = LoggerFactory.getLogger(GRPCVerticle::class.java)
    }

    override suspend fun start() {
        val host = "localhost"
        val port = 9090
        // instead of gRPC ServerBuilder, we use VertxServerBuilder here
        val vertxServer = VertxServerBuilder
            .forAddress(vertx, host, port)
            .addService(ProductService())
            .build()
        vertxServer.coroutineStart()
        logger.info("GRPC verticle started")
    }

    override suspend fun stop() {
        logger.info("GRPC verticle faded")
    }

    private suspend fun VertxServer.coroutineStart(): VertxServer {
        awaitResult<Void> {
            start(it)
        }
        return this
    }
}
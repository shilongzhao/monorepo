package org.szhao

import io.vertx.core.Vertx
import io.vertx.grpc.VertxChannelBuilder
import io.vertx.junit5.VertxExtension
import io.vertx.junit5.VertxTestContext
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.slf4j.LoggerFactory
import proto.Messages
import proto.ProductInfoGrpc
import java.util.concurrent.TimeUnit

@ExtendWith(VertxExtension::class)
class GRPCVerticleTest {
    companion object {
        private val logger = LoggerFactory.getLogger(GRPCVerticleTest::class.java)
    }

    @BeforeEach
    fun setUp(vertx: Vertx, testContext: VertxTestContext) {
        // when a Vertx instance is being created by a @BeforeEach method,
        // it is being closed after possible @AfterEach methods have completed.
        vertx.deployVerticle(MainVerticle(), testContext.succeeding {// we could start directly GRPCVerticle here also
            logger.info("verticles deployment OK")
            testContext.completeNow()
        })
    }

    @AfterEach
    fun tearDown() {
    }

    // TODO: channel close
     @Test
    fun addProductTest(vertx: Vertx, testContext: VertxTestContext) {
        logger.info("add product blocking test")
        val channel = VertxChannelBuilder.forAddress(vertx, "localhost", 9090).usePlaintext().build()
        val stub = ProductInfoGrpc.newBlockingStub(channel) // we use blocking stub here, it's also possible to use non-blocking stub
        val request = Messages.Product.newBuilder().setName("iPhone XR").setDescription("New iPhone model 2018").build()
        val pid = stub.addProduct(request)
        logger.info("received product id ${pid.value}")
        assertNotNull(pid.value)
        val response = stub.getProduct(pid)
        assertEquals(request.name, response.name)
        assertEquals(request.description, response.description)
        channel.shutdown().awaitTermination(10, TimeUnit.SECONDS)
        testContext.completeNow()
    }


}
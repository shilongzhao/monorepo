package org.szhao

import io.vertx.junit5.VertxExtension
import io.vertx.core.Vertx
import io.vertx.junit5.VertxTestContext
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith


@ExtendWith(VertxExtension::class)
class MainVerticleTest {
    @BeforeEach
    fun deployVerticle(vertx: Vertx, testContext: VertxTestContext) {
        vertx.deployVerticle(MainVerticle(), testContext.succeeding<String> {
            testContext.completeNow() // make sure async deploy is successful then start the tests, still java style
        })
    }

    @Test
    fun verticleDeployed(vertx: Vertx, testContext: VertxTestContext) {
        testContext.completeNow()
    }

    @AfterEach
    fun checkVerticle(vertx: Vertx) {
        // when a Vertx instance is being created by a @BeforeEach method,
        // it is being closed after possible @AfterEach methods have completed.
    }
}